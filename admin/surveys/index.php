<?php
    include('../header.php');
?>
    <style type="text/css">
        .actionBar {
            display: none
        }
    </style>
    <div class="wrapper wrapper-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Surveys</h5>
                            <div class="ibox-tools">
                                <a href="javascript:addSurvey()" id="command-add"> <i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="employee_grid" class="table table-hover table-striped" data-toggle="bootgrid">
                                    <thead>
                                        <tr>
                                            <th data-column-id="Id" data-type="numeric" data-identifier="true">Survey Id</th>
                                            <th data-column-id="name">Name</th>
                                            <th data-column-id="Created_Date">Created Date</th>
                                            <th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="add_model" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Employee</h4> </div>
                <div class="modal-body">
                    <form method="post" id="frm_add">
                        <input type="hidden" value="add" name="action" id="action">
                        <input type="hidden" value="<?php echo $_SESSION['login_userId'] ?>" name="Id">
                        <div class="form-group">
                            <label for="name" class="control-label">Name:</label>
                            <input type="text" class="form-control" id="name" name="name" /> </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="btn_add" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <div id="edit_model" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Employee</h4> </div>
                <div class="modal-body">
                    <form method="post" id="frm_edit">
                        <input type="hidden" value="edit" name="action" id="action">
                        <input type="hidden" value="" name="edit_id" id="edit_id">
                        <div class="form-group">
                            <label for="name" class="control-label">Name:</label>
                            <input type="text" class="form-control" id="edit_name" name="edit_name" /> </div>
                        <div class="form-group"> </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="btn_edit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                var grid = $("#employee_grid").bootgrid({
                    ajax: true
                    , rowSelect: true
                    , searchable: false
                    , sortable: false
                    , url: "../survey-response.php"
                    , formatters: {
                        "commands": function (column, row) {
                            return "<button type=\"button\" class=\"btn btn-sm btn-default command-edit command\" data-row-id=\"" + row.Id + "\"><span class=\"glyphicon glyphicon-edit\"></span></button> " + "<button type=\"button\" class=\"btn btn-sm btn-default command-delete command\" data-row-id=\"" + row.Id + "\"><span class=\"glyphicon glyphicon-trash\"></span></button>" + "<button type=\"button\" class=\"btn btn-sm btn-default command-details command\" style=\"margin-left: 3px\" data-row-id=\"" + row.Id + "\"><span class=\"glyphicon glyphicon-log-in\"></span></button>"
                            + "<button type=\"button\" class=\"btn btn-sm btn-default command-copy command\" style=\"margin-left: 3px\" data-row-id=\"" + row.Id + "\"><span class=\"glyphicon glyphicon-link\"></span></button>";
                        }
                    }
                }).on("loaded.rs.jquery.bootgrid", function (response) {
                    /* Executes after data is loaded and rendered */
                    grid.find(".command-edit").on("click", function (e, columns, row) {
                        var ele = $(this).parent();
                        var g_id = $(this).parent().siblings(':first').html();
                        var g_name = $(this).parent().siblings(':nth-of-type(2)').html();
                        $('#edit_model').modal('show');
                        if (g_id > 0) {
                            $('#edit_id').val(g_id); // in case we're changing the key
                            $('#edit_name').val(g_name);
                        }
                    }).end().find(".command-delete").on("click", function (e) {
                        var ele = $(this).parent();
                        var g_id = $(this).parent().siblings(':first').html();
                        var g_name = $(this).parent().siblings(':nth-of-type(2)').html();
                        var conf = confirm('Delete ' + g_id + ' items?');
                        alert(conf);
                        if (conf) {
                            $.post('../survey-response.php', {
                                id: g_id
                                , action: 'delete'
                            }, function (data) {
                                $("#employee_grid").bootgrid('reload');
                            });
                        }
                    }).end().find(".command-details").on("click", function (e) {
                        $.post("../survey-session.php", {
                            "surveyId": $(this).parent().siblings(':first').html()
                        }, function (data) {
                            window.location.href = "../survey-questions/";
                        });
                    }).end().find(".command-copy").on("click", function (e) {
                        var id = $(this).attr('data-row-id');
                        var iframeElem = "<iframe src='" + window.location.origin + "/employer-survey/index.php?survey=" + id + "'></iframe>";
                        copyToClipboard(iframeElem)
                    });
                });

                function copyToClipboard(element) {
                    var $temp = $("<input>");
                    $("body").append($temp);
                    $temp.val(element).select();
                    document.execCommand("copy");
                    $temp.remove();
                }

                function ajaxAction(action) {
                    data = $("#frm_" + action).serializeArray();
                    $.ajax({
                        type: "POST"
                        , url: "../survey-response.php"
                        , data: data
                        , dataType: "json"
                        , success: function (response) {
                            $('#' + action + '_model').modal('hide');
                            $("#employee_grid").bootgrid('reload');
                        }
                        , error: function (response) {
                            
                        }
                    });
                }
                $("#btn_add").click(function () {
                    ajaxAction('add');
                });
                $("#btn_edit").click(function () {
                    ajaxAction('edit');
                });
            });

            function addSurvey() {
                $('#add_model').modal('show');
            }
        </script>
        <?php
    include('../footer.php');
?>