
<?php
//include connection file
include_once ("connection.php");


$db = new dbObj();
$connString = $db->getConnstring();

$params = $_REQUEST;

$action = isset($params['action']) != '' ? $params['action'] : '';
$ansCls = new Result($connString);

switch ($action) {
    case 'getUsers':
        $ansCls->getUsers($params);
        break;
    case 'getSurveys':
        $ansCls->getSurveys($params);
        break;
    case 'getQuestions':
        $ansCls->getQuestions($params);
        break;
    case 'getResults':
        $ansCls->deleteAnswer($params);
        break;
    case 'deleteById':
        $ansCls->deleteAnswerByQuestion($params);
        break;
    default:
        $ansCls->getAnswer($params);
        return;
}

class Result
{
    protected $conn;
    protected $data = array();
    function __construct($connString)
    {
        $this->conn = $connString;
    }

    public function getUsers($params)
    {
        $sql = "Select DISTINCT CompletedByEmail from SurveyResult";

        $queryRecords = mysqli_query($this->conn, $sql);

        while ($row = mysqli_fetch_assoc($queryRecords)) {
            $data[] = $row;
        }

        echo json_encode($data);
    }

    public function getSurveys($params)
    {
        $sql = "SELECT Survey.name, Survey.Id from Survey WHERE Id = (SELECT DISTINCT SurveyResult.SurveyId FROM SurveyResult WHERE SurveyResult.CompletedByEmail = '" .
            $params["email"] . "')";

        $queryRecords = mysqli_query($this->conn, $sql);

        while ($row = mysqli_fetch_assoc($queryRecords)) {
            $data[] = $row;
        }

        echo json_encode($data);
    }

    public function getAnswer($params)
    {

        $this->data = $this->getRecords($params);

        echo json_encode($this->data);
    }

    function getResults($params)
    {

        $rp = isset($params['rowCount']) ? $params['rowCount'] : 10;

        if (isset($params['current'])) {
            $page = $params['current'];
        } else {
            $page = 1;
        }
        ;
        $start_from = ($page - 1) * $rp;

        $sql = $sqlRec = $sqlTot = $where = '';


        // getting total number records without any search
        $sql = "SELECT * FROM Answer WHERE QuestionId=" . $params["quesId"];
        $sqlTot .= $sql;
        $sqlRec .= $sql;

        //concatenate search sql if value exist
        if (isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }
        if ($rp != -1)
            $sqlRec .= " LIMIT " . $start_from . "," . $rp;


        $qtot = mysqli_query($this->conn, $sqlTot) or die("error to fetch tot question data");
        $queryRecords = mysqli_query($this->conn, $sqlRec) or die("error to fetch question data");

        while ($row = mysqli_fetch_assoc($queryRecords)) {
            $data[] = $row;
        }

        $json_data = array(
            "current" => intval($params['current']),
            "rowCount" => 10,
            "total" => intval($qtot->num_rows),
            "rows" => $data // total data array
                );

        return $json_data;
        //        $sql = "SELECT * FROM Answer WHERE QuestionId=" . $params["quesId"];
        //
        //        $queryRecords = mysqli_query($this->conn, $sql);
        //
        //        while( $row = mysqli_fetch_assoc($queryRecords) ) {
        //			$data[] = $row;
        //        }
        //
        //        echo json_encode($data);

        //        $sql = "SELECT Question.Question from Question WHERE Id = (SELECT SurveyQuestion.QuestionId FROM SurveyQuestion WHERE SurveyQuestion.SurveyId = " . $params["survey"] . ")";
        //
        //        $queryRecords = mysqli_query($this->conn, $sql);
        //
        //        while( $row = mysqli_fetch_assoc($queryRecords) ) {
        //			$data[] = $row;
        //        }
        //
        //        echo json_encode($data);

    }

    function getAnswerByQuestion($params)
    {
        $rp = isset($params['rowCount']) ? $params['rowCount'] : 10;

        if (isset($params['current'])) {
            $page = $params['current'];
        } else {
            $page = 1;
        }
        ;
        $start_from = ($page - 1) * $rp;

        $sql = $sqlRec = $sqlTot = $where = '';


        // getting total number records without any search
        $sql = "SELECT * FROM Answer WHERE QuestionId=" . $params["quesId"];
        $sqlTot .= $sql;
        $sqlRec .= $sql;

        //concatenate search sql if value exist
        if (isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }
        if ($rp != -1)
            $sqlRec .= " LIMIT " . $start_from . "," . $rp;


        $qtot = mysqli_query($this->conn, $sqlTot) or die("error to fetch tot question data");
        $queryRecords = mysqli_query($this->conn, $sqlRec) or die("error to fetch question data");

        while ($row = mysqli_fetch_assoc($queryRecords)) {
            $data[] = $row;
        }

        $json_data = array(
            "current" => intval($params['current']),
            "rowCount" => 10,
            "total" => intval($qtot->num_rows),
            "rows" => $data // total data array
                );

        return $json_data;
        //        $sql = "SELECT * FROM Answer WHERE QuestionId=" . $params["quesId"];
        //
        //        $queryRecords = mysqli_query($this->conn, $sql);
        //
        //        while( $row = mysqli_fetch_assoc($queryRecords) ) {
        //			$data[] = $row;
        //        }
        //
        //        echo json_encode($data);
    }


    function getRecords($params)
    {

    }
    function updateAnswer($params)
    {
        $data = array();

        $sql = "Update Answer set Description = '" . $params["answer1"] . "', Weight = " .
            $params["weight1"] . " WHERE Id=" . $params["answerId1"];
        $sql1 = "Update Answer set Description = '" . $params["answer1"] .
            "', Weight = " . $params["weight1"] . ", Id = " . $params["edit_quesId"] .
            " WHERE Id=" . $params["answerId1"];
        $sql2 = "Update Answer set Description = '" . $params["answer1"] .
            "', Weight = " . $params["weight1"] . ", Id = " . $params["edit_quesId"] .
            " WHERE Id=" . $params["answerId1"];
        $sql3 = "Update Answer set Description = '" . $params["answer1"] .
            "', Weight = " . $params["weight1"] . ", Id = " . $params["edit_quesId"] .
            " WHERE Id=" . $params["answerId1"];

        if (mysqli_query($this->conn, $sql)) {
            if (mysqli_query($this->conn, $sql1)) {
                if (mysqli_query($this->conn, $sql2)) {
                    echo mysqli_query($this->conn, $sql3);
                }
            }
        } else {
            echo mysqli_error($conn);
        }

        echo $result = mysqli_query($this->conn, $sql) or die("error to update answer");
    }

    function deleteAnswer($params)
    {
        $data = array();

        $sql = "delete from Answer WHERE Id=" . $params["id"];

        echo $result = mysqli_query($this->conn, $sql) or die("error to delete answer");

    }

    function deleteAnswerByQuestion($params)
    {
        $sql = "delete from Answer WHERE QuestionId=" . $params["quesId"];

        if (mysqli_query($this->conn, $sql)) {
            echo true;
        } else {
            echo mysqli_error($conn);
        }
    }
}
?>
	