
<?php
//include connection file
include ('connection.php');

$db = new dbObj();
$connString = $db->getConnstring();

$params = $_REQUEST;

//print_r($params);

$action = isset($params['action']) != '' ? $params['action'] : '';
$quesCls = new Survey($connString);

switch ($action) {
    case 'insertSurvey':
        $quesCls->insertResult($params, $connString);
        break;
    default:

        return;
}

class Survey
{
    protected $conn;
    protected $data = array();
    function __construct($connString)
    {
        $this->conn = $connString;
    }

    public function getQuestion($params)
    {

        $this->data = $this->getRecords($params);

        echo json_encode($this->data);
    }

    function insertResult($params, $conn)
    {
        $data = array();
        $obtained = 0;
        $total = 0;

        $indexOfParams = 0;

        //insert record in survey result

        $sql = "INSERT INTO SurveyResult ( SurveyId, CompletedByEmail, Company, DateAttended, Phone, NoOfEmployees) VALUES (" .
            $params["surveyId"] . ",'" . $params["email"] . "','" . $params["org_name"] .
            "', NOW() ,'" . $params["phone"] . "','" . $params["no_of_employees"] . "')";

        if (!mysqli_query($this->conn, $sql)) {
            echo mysqli_error($conn);
        }

        $survay_result_id = $conn->insert_id;
       $cat_result = array();
       //print_r($params);
        foreach ($params as $key => $val) {
            if ($indexOfParams >= 6) {
                // $key=$key
                //$lastQue = "";

                if (($pos = strpos($key, '-')) !== false) {
                    $key = substr($key, $pos + 1);
                } else {
                    $key = get_last_word($key);
                }
                // insert into servay result
                $sql1 = "INSERT INTO SurveyResultDetails ( SurveyResultId, QuesId, AnsId, CreatedOn) VALUES (" .
                    $survay_result_id . "," . $key . "," . $val . ", NOW() 
                    )";

                

                if (!mysqli_query($this->conn, $sql1)) {
                    echo mysqli_error($conn);
                }
               

                $sql_type = "select Type from question where Id=" . $key;
                $queryRecords1 = mysqli_query($this->conn, $sql_type);
                //$lastQue="";
                while ($row = mysqli_fetch_assoc($queryRecords1)) {
                    $type = $row['Type'];
                }


                // count the weigth obtained score
                if ($type != 3) {
                    $sql = "SELECT answer.Weight ,question.Type , question.Category ,questioncategory.Type  as Cat from answer join question on question.Id=answer.QuestionId  join questioncategory on questioncategory.Id =question.Category  where question.Id=" .
                        $key . " and answer.Id=" . $val;


                } else {
                    $sql = "SELECT question.Type , question.Category ,questioncategory.Type as Cat from question join questioncategory on questioncategory.Id =question.Category where question.Id=" .
                        $key;

                }

               
                
                
                $queryRecords = mysqli_query($this->conn, $sql);
                $iter=0;
                //$lastQue="";
                while ($row = mysqli_fetch_assoc($queryRecords)) {
                    
                    $type = $row['Type'];
                    $cat = $row['Category'];
                    $cat_name=  $row['Cat'];
                    $cat_total=0;
                    $cat_obtained=0;

                    if ($type == 1) { // put logic of max here for total
                        // count the weigth obtained score
                        // if($lastQue!=$key){
                        $sql_max = "SELECT MAX(Weight) as Weight from answer  where QuestionId=" . $key;
                        //echo $sql_max;
                        $queryReco = mysqli_query($this->conn, $sql_max);
                        while ($row1 = mysqli_fetch_assoc($queryReco)) {
                            $total = $total + $row1['Weight'];
                            $cat_total=$cat_total+$row1['Weight'];
                            //echo "total1  ".$total;
                        }
                        //}
                        $lastQue = $key;
                        $obtained = $obtained + $row['Weight'];
                        $cat_obtained=$cat_obtained+$row['Weight'];

                        //echo "$obtained  ".$obtained;
                    }
                    if ($type == 2) { // keep simple
                        $obtained = $obtained + $row['Weight'];
                        $total = $total + $row['Weight'];
                        $cat_total=$cat_total+$row['Weight'];
                        $cat_obtained=$cat_obtained+$row['Weight'];
                        //echo "total2  ".$total;
                        //echo "$obtained  ".$obtained;
                    }
                    if ($type == 3) { // here add just value
                        $obtained = $obtained + $val;
                        $total = $total + $val;
                        $cat_total=$cat_total+$val;
                        $cat_obtained=$cat_obtained+$val;
                        //echo "total3  ".$total;
                        //echo "$obtained  ".$obtained;
                    }
                    
                    $cat_result[$cat][]=array("CatName"=>$cat_name,"Cat"=>$cat,"Total" => $cat_total, "Obtained" => $cat_obtained);
                    
                }


            }

            $indexOfParams++;
        }

        $cat_result['total'][]=array("CatName"=>'Total',"Cat"=>'Total',"Total" => $total, "Obtained" => $obtained);
       // $res = array("Total" => $total, "Obtained" => $obtained);
        echo json_encode($cat_result);


    }


    function getSurvey($params)
    {

        // getting total number records without any search
        $sql = "SELECT Q.Id AS QuestionID, Q.Question AS QuesDescription, Q.Type AS Type, A.Id AS AnswerId, A.Description AS Answer, A.Weight FROM Question Q JOIN Answer A ON A.QuestionId=Q.Id JOIN SurveyQuestion SQ ON SQ.QuestionId=Q.Id WHERE SQ.SurveyId=" .
            $params['surveyId'];
        $queryRecords = mysqli_query($this->conn, $sql);

        while ($row = mysqli_fetch_assoc($queryRecords)) {
            $data[] = $row;
        }

        echo json_encode($data);

    }
}
?>
	