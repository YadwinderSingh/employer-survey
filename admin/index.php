<?php
   include("connection.php");
   session_start();

    $db = new dbObj();
	$connString =  $db->getConnstring();
   
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 
      
      $myusername = $_POST['username'];
      $mypassword = $_POST['password']; 
      
      $sql = "SELECT * FROM User WHERE username = '$myusername' and passcode = '$mypassword'";
      $result = mysqli_query($connString, $sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      
      $count = mysqli_num_rows($result);
         
      // If result matched $myusername and $mypassword, table row must be 1 row
       
      if($count == 1) {
        $_SESSION['login_user'] = $myusername;
        $_SESSION['login_userId'] = $row['Id'];
         
         header("location: ../admin/home/");
      }else {
         $error = "Your Login Name or Password is invalid";
      }
   }
?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Employer Tool | Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div class="box-container">
            <h3>Welcome to Employer Tool</h3>
            <p>Login. To see it in action.</p>
            <form class="m-t" role="form" method="post" action="">
                <div class="form-group">
                    <input type="text" class="form-control" name="username" placeholder="Username" required="">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                <div style = "font-size:12px; color:#cc0000; margin-top:10px"><?php echo $result; ?></div>
            </form>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
