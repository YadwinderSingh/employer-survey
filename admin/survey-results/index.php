<?php
    include('../session.php');
    
    $directoryURI = $_SERVER['REQUEST_URI'];
    $path = parse_url($directoryURI, PHP_URL_PATH);
    $components = explode('/', $path);
    $active = $components[2];
    
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Employer Tool</title>
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="../css/animate.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        <!-- Mainly scripts -->
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="../js/bootstrap.min.js"></script>
    </head>

    <body class="top-navigation">
        <div id="wrapper">
            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom white-bg">
                    <nav class="navbar navbar-static-top" role="navigation">
                        <div class="navbar-header">
                            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button"> <i class="fa fa-reorder"></i> </button> <a href="javascript:void()" class="navbar-brand">EmployerTool</a> </div>
                        <div class="navbar-collapse collapse" id="navbar">
                            <ul class="nav navbar-nav">
                                <?php
                                echo '<li ';                           
                                
                                if($active == 'Dashboard') {echo 'class="active"';} 
                                echo '><a href="../Dashboard/">Dashboard</a></li><li ';
                                if($active == 'surveys') {echo 'class="active"';} 
                                echo '><a href="../surveys/">Surveys</a></li><li ';
                                if($active == 'survey-questions') {echo 'class="active"';} 
                                echo '><a href="../survey-questions/">Survey Questions</a></li><li ';
                                if($active == 'survey-results') {echo 'class="active"';} 
                                echo '><a href="../survey-results/">Survey Results</a></li>';
                            ?>
                            </ul>
                            <ul class="nav navbar-top-links navbar-right">
                                <li><a href="javascript:void()">Welcome <?php  echo $login_session ?></a></li>
                                <li>
                                    <a href="/employer-tool-dashboard/logout.php"> <i class="fa fa-sign-out"></i> Log out </a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <script src="http://onlinetestguru.com/js\plugin\datatables\jquery.dataTables.min.js"></script>
                <script src="http://onlinetestguru.com/js\plugin\datatables\dataTables.bootstrap.min.js"></script>
                <script src="http://onlinetestguru.com/js\plugin\datatables\dataTables.colReorder.min.js"></script>
                <style type="text/css">
                    .actionBar {
                        display: none
                    }
                </style>
                <div class="wrapper wrapper-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Results</h5> </div>
                                    <div class="ibox-content">
                                        <div class="form-group">
                                            <label for="surveys" class="control-label">User:</label>
                                            <select class="form-control" name="surveys" id="users"> </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="surveys" class="control-label">Surveys:</label>
                                            <select class="form-control" name="surveys" id="surveys"> </select>
                                        </div>
                                        <div class="table-responsive">
                                            <table id="result_grid" class="table table-hover table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Question</th>
                                                        <th>View Answer</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    var answerResponse;
                    var answerAdded = true;
                    var action = 'add'
                    $(document).ready(function () {
                        $.post("../result-response.php", [{
                            name: "action"
                            , value: "getUsers"
            }], function (surveyResponse) {
                            surveyResponse = jQuery.parseJSON(surveyResponse);
                            if (surveyResponse != null) {
                                $.each(surveyResponse, function (i, e) {
                                    $('#users').append($('<option>', {
                                        value: e.CompletedByEmail
                                        , text: e.CompletedByEmail
                                    }));
                                });
                                loadSurveyDropDown();
                            }
                        });
                        $("#btn_add").click(function () {
                            $('#frm_answer_addedit #action').val('add');
                            ajaxAction(action);
                        });
                        $("#addedit_type").change(function () {
                            hideShowForm($(this).val());
                        });
                        $("#surveys").change(function () {
                            var usermail = $("#users option:selected").attr("value");
                            var surveyId = $("#surveys option:selected").attr("value");
                            debugger;
                            //$("#targettest_caty").html( "" );
                            //if (testId.length > 0  && testId!="default" ) { 
                            var table = $("#result_grid").DataTable();
                            ///$("#table_s").removeClass("hidden");
                            table.destroy();
                            var url = "../fetchQuestions.php?usermail=" + $("#users").val() + "&surveyId=" + $("#surveys").val();
                            //$.fn.dataTable.ext.errMode = "none";
                            $("#result_grid").dataTable({
                                "bProcessing": true
                                , "sAjaxSource": url
                                , "aoColumns": [

                                    {
                                        sWidth: "75%"
                                        , mData: "Question"
                                    }
                                    , {
                                        sWidth: "25%"
                                        , "mRender": function (data, type, oObj) {
                                            debugger;
                                            return '<button type="button" class="btn btn-sm btn-default  command-details" onclick="editTest(' + oObj.Id + ');" style="margin-left: 3px" data-row-id="undefined"><span class="glyphicon glyphicon-log-in"></span></button>'
                                        }
                },

               ]
                            });
                            $("#result_grid_filter>label>span").addClass('hidden');
                            $("#result_grid_length").css("float", "right");
                        });
                        $("#users").change(function () {
                            loadSurveyDropDown();
                        });
                        $('#addedit_modal').on('hidden.bs.modal', function () {
                            $("#frm_addedit").trigger("reset");
                            $("#frm_answer_addedit").trigger("reset");
                        });
                    });

                    function loadSurveyDropDown() {
                        $.post("../result-response.php", [{
                            name: "action"
                            , value: "getSurveys"
            }, {
                            name: "email"
                            , value: $('#users').val()
            }], function (surveyResponse) {
                            surveyResponse = jQuery.parseJSON(surveyResponse);
                            if (surveyResponse != null) {
                                $.each(surveyResponse, function (i, e) {
                                    $('#surveys').empty();
                                    $('#surveys').append($('<option>', {
                                        value: e.Id
                                        , text: e.name
                                    }));
                                });
                                $.post("../survey-session.php?surveyId=" + $("#surveys").val(), [{
                                    "surveyId": $("#surveys").val()
                                }], function (data) {
                                    //$("#result_grid").DataTable().ajax.reload();
                                    // $("#result_grid").bootgrid('reload');
                                });
                            }
                        });
                    }
                    // open edit po up 
                    function editTest(id) {
                        var Id = id;
                        //$id
                        $.ajax({
                            url: '../fetchAnswers.php'
                            , type: 'POST'
                            , dataType: 'json'
                            , data: {
                                'id': Id
                            }
                            , cache: false
                            , success: function (result) {
                                debugger;
                                $("#result").html(result);
                                $("#exampleModal").modal();
                            }
                            , error: function (XMLHttpRequest, textStatus, errorThrown) {
                                $("#result").html(XMLHttpRequest.responseText);
                                $("#exampleModal").modal();
                            }
                        });
                    }
                </script>
                <div id="exampleModal" class="modal fade" role="dialog" aria-hidden="true" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <div class="modal-content" id="modal-contentEditExamCat">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title" id="exampleModalLabel">Answer Detail</h4> </div>
                            <div class="modal-body">
                                <form>
                                    <div class="row" id="result"> </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
    
    
    
    include('../footer.php');
?>