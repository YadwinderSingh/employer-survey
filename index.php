<?php
    include('connection.php');
    $surveyId = htmlspecialchars($_GET["survey"]);    
?> 
<!DOCTYPE html>
<html lang="en">

<head>
 <meta property="og:url"           content="http://www.your-domain.com/your-page.html" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="Your Website Title" />
  <meta property="og:description"   content="Your description" />
  <meta property="og:image"         content="http://www.your-domain.com/path/image.jpg" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Lovell | Employer Tool</title>
    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
    <!-- BASE CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/icon_fonts/css/all_icons_min.css" rel="stylesheet">
    <link href="css/skins/square/grey.css" rel="stylesheet">
    
    <script src="js/modernizr.js"></script>
    <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '116778292316311',
      xfbml      : true,
      version    : 'v2.10'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

    <!-- Modernizr -->
</head>
<body>
    <div id="preloader">
        <div data-loader="circle-side"></div>
    </div>
    <!-- /Preload -->
    <main>
        <div id="form_container">
            <div class="col-md-12">
                    <div id="wizard_container">
                        <div id="top-wizard">
                            <div id="progressbar"></div>
                        </div>

                        <!-- /top-wizard -->
                        <form name="survey_form" id="survey_form" method="POST">
                            <div id="middle-wizard">

                            <?php 

                                $db = new dbObj();
                                $connString =  $db->getConnstring();
                                $noOfPages = 1;

                                $sql = "SELECT Q.Id AS QuestionID, Q.Question AS QuesDescription, Q.Type AS Type FROM Question Q JOIN SurveyQuestion SQ ON SQ.QuestionId=Q.Id WHERE SQ.SurveyId=" . $surveyId. " order by Q.Type desc";
                                
                                $questions = mysqli_query($connString, $sql) or die (mysql_error());

                                $noOfQues = intval($questions->num_rows);

                                if($noOfQues < 10){
                                    $noOfPages += 1;
                                }
                                else if($noOfQues > 10){
                                    $noOfPages = ceil($noOfQues/10) + 1;
                                }


                            ?>

                            <?php

                                error_reporting(0);
                                $pageNumber = 1;
                                $questionNumber = 1;

                                while ($question = mysqli_fetch_assoc($questions)) {

                                    $questionType = intval($question['Type']);

                                    if($questionNumber == 1){
                                        echo '<div class="step';
                                        echo '"><h3 class="step-heading"><strong>';
                                        echo $pageNumber;
                                        echo '/';
                                        echo $noOfPages;
                                        if($questionType != 3){ 
                                            echo '</strong></h3><div class="col-sm-12">';
                                        }
                                        else{
                                            echo '</strong>Use the slider to indicate how well each of the statements below describe your organization from the perspective of young employees.</h3>
                                                <div class="head-container">
                                                    <ul class="labels">
                                                        <li> <span>Not at all</span> </li>
                                                        <li> <span>Somewhat</span> </li>
                                                        <li> <span>Very Well</span> </li>
                                                    </ul>
                                                    
                                                    <ul class="numbers">
                                                        <li> 0 </li>
                                                        <li> 1 </li>
                                                        <li> 2 </li>
                                                        <li> 3 </li>
                                                        <li> 4 </li>
                                                        <li> 5 </li>
                                                        <li> 6 </li>
                                                        <li> 7 </li>
                                                        <li> 8 </li>
                                                        <li> 9 </li>
                                                        <li> 10 </li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-12">';
                                        }
                                    }

                                    $questionIndex = $questionNumber;
                                    if($pageNumber > 1){
                                        $questionIndex = (($pageNumber - 1) * 10) + $questionNumber;
                                    }
                                    

                                    echo '<div class="row"><h4 class="main_question"><span>' . $questionIndex . '. ';
                                    echo $question['QuesDescription'];
                                    echo '</span></h4>';

                                    if($questionType != 3){
                                        $sqlAnswer = "SELECT * FROM Answer A WHERE A.QuestionId=" . $question['QuestionID'];
                                        $answers = mysqli_query($connString, $sqlAnswer);
                                    }

                                    switch($questionType){
                                        case 1:
                                            while($answer = mysqli_fetch_assoc($answers)) {
                                                echo '<div class="form-group radio_input">
                                                    <label><input type="radio" value="';
                                                echo $answer['Id'];
                                                echo '" id="ques_' . $answer['QuestionId'] .'" name="question_';
                                                echo $answer['QuestionId'];
                                                echo '" class="icheck required" data-weight="';
                                                echo $answer['Weight'] . '">';
                                                echo $answer['Description'];
                                                echo '</label></div>';
                                            }
                                            break;
                                        case 2:
                                            while($answer = mysqli_fetch_assoc($answers)) {
                                                echo '<div class="form-group radio_input">
                                                    <label><input type="checkbox" value="';
                                                echo $answer['Id'];
                                                echo '" id="ques_' . $answer['QuestionId'] .'" name="question_';
                                                echo $answer['QuestionId'] . '[]';
                                                echo '" class="icheck required" data-weight="';
                                                echo $answer['Weight'] . '">';
                                                echo $answer['Description'];
                                                echo '</label></div>';
                                            }
                                            break;
                                        case 3:
                                            echo '<div class="form-group"><input type="range" name="question_';
                                            echo $question['QuestionID'];
                                            echo '" min="0" max="10" value="0" /> </div>';
                                            break;

                                            mysqli_free_result($answers);
                                    }

                                    echo '</div>';

                                    if($questionNumber == 10 || ((($pageNumber - 1) * 10) + $questionNumber) == $noOfQues){
                                        echo '</div></div>';
                                        $pageNumber++;
                                    }

                                    if($questionNumber == 10){
                                        $questionNumber = 1;
                                    }
                                    else {
                                        $questionNumber++;
                                    }
                                }

                                mysqli_free_result($questions);
                            ?>
                            
                            <div class="step submit">
                                <h3 class="step-heading"><strong><?php echo $pageNumber ?>/ <?php echo $noOfPages ?> </strong>Please fill with details of your target employees</h3>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="hidden" name="surveyId" value="<?php echo $surveyId; ?>">
                                            <input type="text" name="org_name" class="form-control required" placeholder="Your Organisation Name"> </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="email" name="email" class="form-control required" placeholder="Your Email"> 
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="phone" name="phone" class="form-control required" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="number" name="no_of_employees" class="form-control required" placeholder="Number of employees"> 
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div>
                            <!-- /middle-wizard -->
                            <div id="bottom-wizard">
                                <button type="button" name="backward" class="backward">Backward </button>
                                <button type="button" name="forward" class="forward">Forward</button>
                                <button type="submit" name="process" class="submit">Submit</button>
                            </div>
                            <!-- /bottom-wizard -->
                        </form>
                    </div>
                    <!-- /Wizard container -->
                    
                </div>
            
            
            <div id="success" class="hidden">
                <div class="icon icon--order-success svg">
                    <svg xmlns="http://www.w3.org/2000/svg" width="72px" height="72px">
                        <g fill="none" stroke="#8EC343" stroke-width="2">
                        <circle cx="36" cy="36" r="35" style="stroke-dasharray:240px, 240px; stroke-dashoffset: 480px;"></circle>
                        <path d="M17.417,37.778l9.93,9.909l25.444-25.393" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;"></path>
                    </g>
                </svg>
                </div>
                <h4><span>Survey completed successfully!</span></h4>
                <h3 id="total"></h3>
                
                <div class="contact-share">
                    <button type="button" class="btn_1">Contact Lovell Corporation</button>
                    <div class="social light">
                        <a class="facebook" href="https://github.com/yadwinderpalsingh" target="_blank">
                            <i class="fa icon-facebook"></i>
                        </a>
                        <a class="twitter" href="https://twitter.com/1Yadwinder" target="_blank">
                            <i class="fa icon-twitter"></i>
                        </a>
                        <!-- <a class="linkedin" href="https://www.linkedin.com/in/yadwinderpalsingh/" target="_blank">
                            <i class="fa icon-linkedin"></i>
                        </a> -->
                    </div>
                    <p>Share your score over social media!</p>
                </div>
              
                
            </div>
        </div>
        <!-- /Form_container -->
        
        
          <div
  <div class="fb-share-button" 
    data-href="http://localhost/employer-tool/index.php?survey=8" 
    data-layout="button_count">
  </div>
    </main>
    
    
    
    <footer id="home" class="clearfix">
		<p>© 2017 | Lovell Corporation</p>
		<ul>
			<li><a href="#0" class="animated_link">Terms and conditions</a></li>
			<li><a href="#0" class="animated_link">Contact us</a></li>
		</ul>
    </footer>
    <input id="surveyId" type="hidden" value="<?php $survey ?>"/>
	<!-- end footer-->
    
    <!-- SCRIPTS -->
    <!-- Jquery-->
    <script src="js/jquery-2.2.4.min.js"></script>
    <!-- Common script -->
    <script src="js/common_scripts_min.js"></script>
    <!-- Wizard script -->
    <script src="js/questionare_wizard_func.js"></script>
    <!-- Theme script -->
    <script src="js/functions.js"></script>




</body>

</html>