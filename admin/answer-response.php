
<?php
//include connection file
include_once ("connection.php");
$db = new dbObj();
$connString = $db->getConnstring();
$params = $_REQUEST;
$action = isset($params['action']) != '' ? $params['action'] : '';
$ansCls = new Answer($connString);

switch ($action) {
    case 'add':
        $ansCls->insertAnswer($params);
        break;
    case 'getById':
        $ansCls->getAnswerByQuestion($params);
        break;
    case 'edit':
        $ansCls->updateAnswer($params);
        break;
    case 'delete':
        $ansCls->deleteAnswer($params);
        break;
    case 'deleteById':
        $ansCls->deleteAnswerByQuestion($params);
        break;
    default:
        $ansCls->getAnswer($params);
        return;
}

class Answer
{
    protected $conn;
    protected $data = array();
    function __construct($connString)
    {
        $this->conn = $connString;
    }

    public function getAnswer($params)
    {

        $this->data = $this->getRecords($params);

        echo json_encode($this->data);
    }

    function insertAnswer($params)
    {

        //echo $params["answer1"]

        $data = array();

        $sql = "INSERT INTO Answer (Description, Weight, QuestionId) VALUES('" . $params["answer1"] .
            "'," . $params["weight1"] . "," . $params["quesId"] . ");";
        $sql1 = "INSERT INTO Answer (Description, Weight, QuestionId) VALUES('" . $params["answer2"] .
            "'," . $params["weight2"] . "," . $params["quesId"] . ");";
        $sql2 = "INSERT INTO Answer (Description, Weight, QuestionId) VALUES('" . $params["answer3"] .
            "'," . $params["weight3"] . "," . $params["quesId"] . ");";
        $sql3 = "INSERT INTO Answer (Description, Weight, QuestionId) VALUES('" . $params["answer4"] .
            "'," . $params["weight4"] . "," . $params["quesId"] . ");";

        if (mysqli_query($this->conn, $sql)) {
            if (mysqli_query($this->conn, $sql1)) {
                if (mysqli_query($this->conn, $sql2)) {
                    echo mysqli_query($this->conn, $sql3);
                }
            }
        } else {
            echo mysqli_error($conn);
        }
    }

    function getAnswerByQuestion($params)
    {
        $sql = "SELECT * FROM Answer WHERE QuestionId=" . $params["quesId"];
        //echo $sql;
        $queryRecords = mysqli_query($this->conn, $sql);
        $data = null;
        while ($row = mysqli_fetch_assoc($queryRecords)) {
            $data[] = $row;
        }

        echo json_encode($data);
    }


    function getRecords($params)
    {
        $rp = isset($params['rowCount']) ? $params['rowCount'] : 10;

        if (isset($params['current'])) {
            $page = $params['current'];
        } else {
            $page = 1;
        }
        ;
        $start_from = ($page - 1) * $rp;

        $sql = $sqlRec = $sqlTot = $where = '';


        // getting total number records without any search
        $sql = "SELECT * FROM Answer ";
        $sqlTot .= $sql;
        $sqlRec .= $sql;

        //concatenate search sql if value exist
        if (isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }
        if ($rp != -1)
            $sqlRec .= " LIMIT " . $start_from . "," . $rp;


        $qtot = mysqli_query($this->conn, $sqlTot) or die("error to fetch tot question data");
        $queryRecords = mysqli_query($this->conn, $sqlRec) or die("error to fetch question data");

        while ($row = mysqli_fetch_assoc($queryRecords)) {
            $data[] = $row;
        }

        $json_data = array(
            "current" => intval($params['current']),
            "rowCount" => 10,
            "total" => intval($qtot->num_rows),
            "rows" => $data // total data array
                );

        return $json_data;
    }

    function updateAnswer($params)
    {
        $data = array();


        $sql = "Update Answer set Description = '" . $params["answer1"] . "', Weight = " .
            $params["weight1"] . " WHERE Id=" . $params["answerId1"];
        $result = mysqli_query($this->conn, $sql);
        $sql = "Update Answer set Description = '" . $params["answer2"] . "', Weight = " .
            $params["weight2"] . " WHERE Id=" . $params["answerId2"];
        $result = mysqli_query($this->conn, $sql);
        $sql = "Update Answer set Description = '" . $params["answer3"] . "', Weight = " .
            $params["weight3"] . " WHERE Id=" . $params["answerId3"];
        $result = mysqli_query($this->conn, $sql);
        $sql = "Update Answer set Description = '" . $params["answer4"] . "', Weight = " .
            $params["weight4"] . " WHERE Id=" . $params["answerId4"];


        echo $result = mysqli_query($this->conn, $sql) or die("error to update answer");
    }

    function deleteAnswer($params)
    {
        $data = array();

        $sql = "delete from Answer WHERE Id=" . $params["id"];

        echo $result = mysqli_query($this->conn, $sql) or die("error to delete answer");

    }

    function deleteAnswerByQuestion($params)
    {
        $sql = "delete from Answer WHERE QuestionId=" . $params["quesId"];

        if (mysqli_query($this->conn, $sql)) {
            echo true;
        } else {
            echo mysqli_error($conn);
        }
    }
}
?>
	