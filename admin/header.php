<?php
    include('session.php');
    
    $directoryURI = $_SERVER['REQUEST_URI'];
    $path = parse_url($directoryURI, PHP_URL_PATH);
    $components = explode('/', $path);
    $active = $components[2];
    
?>
        
    <!DOCTYPE html>
    <html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Employer Tool</title>

        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="../css/animate.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">

        <!-- Mainly scripts -->
        <script src="../js/jquery-3.1.1.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/jquery.bootgrid.min.js"></script>
        
    </head>
        
    <body class="top-navigation">

    <div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom white-bg">
                <nav class="navbar navbar-static-top" role="navigation">
                    <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <i class="fa fa-reorder"></i>
                        </button>
                        <a href="javascript:void()" class="navbar-brand">EmployerTool</a>
                    </div>
                    <div class="navbar-collapse collapse" id="navbar">
                        <ul class="nav navbar-nav">
                            <?php
                                echo '<li ';                           
                                
                                if($active == 'Dashboard') {echo 'class="active"';} 
                                echo '><a href="../Dashboard/">Dashboard</a></li><li ';
                                if($active == 'surveys') {echo 'class="active"';} 
                                echo '><a href="../surveys/">Surveys</a></li><li ';
                                if($active == 'survey-questions') {echo 'class="active"';} 
                                echo '><a href="../survey-questions/">Survey Questions</a></li><li ';
                                if($active == 'survey-results') {echo 'class="active"';} 
                                echo '><a href="../survey-results/">Survey Results</a></li>';
                            ?>
                        </ul>
                        <ul class="nav navbar-top-links navbar-right">
                            <li><a href="javascript:void()">Welcome <?php  echo $login_session ?></a></li>
                            <li>
                                <a href="../logout.php">
                                    <i class="fa fa-sign-out"></i> Log out
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

   