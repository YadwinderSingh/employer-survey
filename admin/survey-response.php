<?php
	//include connection file 
	include_once("connection.php");

	
	$db = new dbObj();
	$connString =  $db->getConnstring();

	$params = $_REQUEST;
	
	$action = isset($params['action']) != '' ? $params['action'] : '';
	$empCls = new Employee($connString);

	switch($action) {
        case 'getSurveys':
            $empCls->getSurveyList();
            break;
        case 'add':
            $empCls->insertEmployee($params);
            break;
        case 'edit':
            $empCls->updateEmployee($params);
            break;
        case 'delete':
            $empCls->deleteEmployee($params);
            break;
        default:
            $empCls->getEmployees($params);
        return;
	}
	
	class Employee {
	protected $conn;
	protected $data = array();
	function __construct($connString) {
		$this->conn = $connString;
	}
	
	public function getEmployees($params) {
		
		$this->data = $this->getRecords($params);
		
		echo json_encode($this->data);
	}
        
    public function getSurveyList() {
		
		$sql = "SELECT Survey.Id AS Id, Survey.name AS name FROM Survey";
        
        $queryRecords = mysqli_query($this->conn, $sql);
        
        while( $row = mysqli_fetch_assoc($queryRecords) ) { 
			$data[] = $row;
        }
        
        echo json_encode($data);
	}
        
	function insertEmployee($params) {
		$data = array();
        //$datetime = date("Y-m-d H:i:s");
        
        
		$sql = "INSERT INTO `Survey` (name, Created_By, Created_Date) VALUES('" . $params["name"] . "'," . $params["Id"] . ", NOW());";
		
        //echo $sql;
		echo $result = mysqli_query($this->conn, $sql) or die("error to insert employee data");
		
	}
	
	
	function getRecords($params) {
		$rp = isset($params['rowCount']) ? $params['rowCount'] : 10;
		
		if (isset($params['current'])) { $page  = $params['current']; } else { $page=1; };  
        $start_from = ($page-1) * $rp;
		
		$sql = $sqlRec = $sqlTot = $where = '';
		

	   // getting total number records without any search
		$sql = "SELECT * FROM `survey` ";
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		if ($rp!=-1)
		$sqlRec .= " LIMIT ". $start_from .",".$rp;
		
		
		$qtot = mysqli_query($this->conn, $sqlTot) or die("error to fetch tot employees data");
		$queryRecords = mysqli_query($this->conn, $sqlRec) or die("error to fetch employees data");
		
		while( $row = mysqli_fetch_assoc($queryRecords) ) { 
			$data[] = $row;
		}

		$json_data = array(
			"current"            => intval($params['current']), 
			"rowCount"            => 10, 			
			"total"    => intval($qtot->num_rows),
			"rows"            => intval($qtot->num_rows) > 0 ? $data : []  // total data array
			);
		
		return $json_data;
	}
	function updateEmployee($params) {
		$data = array();
		//print_R($_POST);die;
        $datetime = date("Y-m-d H:i:s");
		$sql = "Update `survey` set name = '" . $params["edit_name"] . "' WHERE id='".$params["edit_id"]."'";
		
		echo $result = mysqli_query($this->conn, $sql) or die("error to update employee data");
	}
	
	function deleteEmployee($params) {
		$data = array();
		//print_R($_POST);die;
        
		$sql = "delete from `survey` WHERE id=".$params["id"];
		
		echo $result = mysqli_query($this->conn, $sql) or die("error to delete employee data");
        
	}
}
?>