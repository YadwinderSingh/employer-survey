<?php
    include('../header.php');    
    $selected_Survey = $_SESSION['selected_survey'];
?>
    <style type="text/css">
        .actionBar {
            display: none
        }
    </style>
    <div class="wrapper wrapper-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Questions</h5>
                            <div class="ibox-tools"> <a href="javascript:addQuestion()" id="command-add"><i class="fa fa-plus"></i></a> </div>
                        </div>
                        <div class="ibox-content">
                            <div class="form-group">
                                <label for="surveys" class="control-label">Survey:</label>
                                <select class="form-control" name="surveys" id="surveys"> </select>
                            </div>
                            <div class="table-responsive">
                                <table id="question_grid" class="table table-hover table-striped" data-toggle="bootgrid">
                                    <thead>
                                        <tr>
                                            <th data-column-id="Id" data-type="numeric" data-identifier="true">Id</th>
                                            <th data-column-id="Question">Question</th>
                                            <th data-visible="false" data-column-id="Type">Type</th>
                                            <th data-column-id="TypeName">Type</th>
                                            <th data-column-id="commands" data-formatter="commands" data-sortable="false">Actions</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="addedit_modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Question</h4> </div>
                <div class="modal-body">
                    <form method="post" id="frm_addedit">
                        <input type="hidden" value="add" name="action" id="action">
                        <input type="hidden" value="" name="addedit_id" id="addedit_id">
                        <div class="form-group">
                            <label for="addedit_question" class="control-label">Question:</label>
                            <textarea class="form-control" required id="addedit_question" name="addedit_question"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="type" class="control-label">Category:</label>
                            <select id="addedit_category" class="form-control" name="addedit_category">
                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="type" class="control-label">Type:</label>
                            <select id="addedit_type" class="form-control" name="addedit_type">
                                
                            </select>
                        </div>
                    </form>
                    <form method="post" id="frm_answer_addedit">
                        <input type="hidden" value="" name="action" id="action">
                        <!--                        <input type="hidden" value="" name="quesId" id="quesId">-->
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="hidden" id="answerId1" name="answerId1" />
                                    <label for="question" class="control-label">Answer 1:</label>
                                    <textarea rows="1" class="form-control" required id="answer1" name="answer1"></textarea>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="weight1" class="control-label">Weight:</label>
                                    <input type="number" min="0" required class="form-control" id="weight1" name="weight1" /> </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="hidden" id="answerId2" name="answerId2" />
                                    <label for="answer2" class="control-label">Answer 2:</label>
                                    <textarea rows="1" class="form-control" required id="answer2" name="answer2"></textarea>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="weight2" class="control-label">Weight:</label>
                                    <input type="number" min="0" required class="form-control" id="weight2" name="weight2" /> </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="hidden" id="answerId3" name="answerId3" />
                                    <label for="answer3" class="control-label">Answer 3:</label>
                                    <textarea rows="1" class="form-control" required id="answer3" name="answer3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="weight3" class="control-label">Weight:</label>
                                    <input type="number" min="0" class="form-control" required id="weight3" name="weight3" /> </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="hidden" id="answerId4" name="answerId4" />
                                    <label for="answer4" class="control-label">Answer 4:</label>
                                    <textarea rows="1" class="form-control" id="answer4" required name="answer4"></textarea>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="weight4" class="control-label">Weight:</label>
                                    <input type="number" min="0" class="form-control" required id="weight4" name="weight4" /> </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="btn_add" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var answerResponse;
        var answerAdded = true;
        var action = 'add'
        $(document).ready(function () {
            $.post("../survey-response.php", [{
                name: "action"
                , value: "getSurveys"
            }], function (surveyResponse) {
                surveyResponse = jQuery.parseJSON(surveyResponse);
                if (surveyResponse != null) {
                    $.each(surveyResponse, function (i, e) {
                        $('#surveys').append($('<option>', {
                            value: e.Id
                            , text: e.name
                        }));
                    });
                    $.post("../get-survey-session.php", function(session){
                        if (session == "") {
                            $("#surveys").val($("#surveys option:first").val());
                            $.post("../survey-session.php", {"surveyId": $("#surveys").val() }, function(data){
                                loadCategories();
                            });
                        }
                        else {
                            $("#surveys").val(parseInt(<?php echo $_SESSION['selected_survey'] ?>));
                            loadCategories();
                        }
                    });
                }
            });
            
            $("#btn_add").click(function () {
                $('#frm_answer_addedit #action').val('add');
                ajaxAction(action);
            });
            $("#addedit_type").change(function () {
                hideShowForm($(this).val());
            });
            
            $("#surveys").change(function () {
                $.post("../survey-session.php", {"surveyId": $("#surveys").val() }, function(data){
                    $("#question_grid").bootgrid('reload');
                });
            });
            
            $('#addedit_modal').on('hidden.bs.modal', function () {
                $("#frm_addedit").trigger("reset");
                $("#frm_answer_addedit").trigger("reset");
            });
        });
        
        function loadCategories(){
            $.post("../question-response.php", [{
                name: "action"
                , value: "getCategoryList"
            }], function (categoryResponse) {
                categoryResponse = jQuery.parseJSON(categoryResponse);
                if (categoryResponse != null) {
                    $.each(categoryResponse, function (i, e) {
                        $('#addedit_category').append($('<option>', {
                            value: e.Id
                            , text: e.Category
                        }));
                    });
                    
                    
                }
                
                loadTypes();
            });
        }
        
        
        function loadTypes(){
            $.post("../question-response.php", [{
                name: "action"
                , value: "getTypeList"
            }], function (typeResponse) {
                typeResponse = jQuery.parseJSON(typeResponse);
                if (typeResponse != null) {
                    $.each(typeResponse, function (i, e) {
                        $('#addedit_type').append($('<option>', {
                            value: e.Id
                            , text: e.Type
                        }));
                    });
                }
                
                loadQuestions();
            });
        }

        function ajaxAction(action) {
            if (!$("#frm_addedit")[0].checkValidity()) {
                $("#frm_addedit")[0].reportValidity()
                if (answerAdded) {
                    if (!$("#frm_answer_addedit")[0].checkValidity()) {
                        $("#frm_answer_addedit")[0].reportValidity()
                    }
                }
                return;
            }
            if (answerAdded) {
                if (!$("#frm_answer_addedit")[0].checkValidity()) {
                    $("#frm_answer_addedit")[0].reportValidity()
                    return;
                }
            }
            data = $("#frm_addedit").serializeArray();
            $.ajax({
                type: "POST"
                , url: "../question-response.php"
                , data: data
                , dataType: "json"
                , success: function (questionId) {
                    debugger;
                    if (answerAdded) {
                        if ($('#answerId1').val() != "") {
                            action = 'edit';
                        }
                        else {
                            action = 'add';
                        }
                        debugger;
                        answerData = [
                            {
                                name: "action"
                                , value: action
                                }
                                , {
                                name: "quesId"
                                , value: questionId
                                }
                                , {
                                name: "answerId1"
                                , value: $('#answerId1').val()
                                }
                                , {
                                name: "answer1"
                                , value: $('#answer1').val()
                                }
                                , {
                                name: "weight1"
                                , value: $('#weight1').val()
                                }
                                , {
                                name: "answerId2"
                                , value: $('#answerId2').val()
                                }
                                , {
                                name: "answer2"
                                , value: $('#answer2').val()
                                }
                                , {
                                name: "weight2"
                                , value: $('#weight2').val()
                                }
                                , {
                                name: "answerId3"
                                , value: $('#answerId3').val()
                                }
                                , {
                                name: "answer3"
                                , value: $('#answer3').val()
                                }
                                , {
                                name: "weight3"
                                , value: $('#weight3').val()
                                }
                                , {
                                name: "answerId4"
                                , value: $('#answerId4').val()
                                }
                                , {
                                name: "answer4"
                                , value: $('#answer4').val()
                                }
                                , {
                                name: "weight4"
                                , value: $('#weight4').val()
                                }
                            ];
                        $.ajax({
                            type: "POST"
                            , url: "../answer-response.php"
                            , data: answerData
                            , dataType: "json"
                            , success: function (response) {
                                debugger;
                                $("#frm_addedit").trigger("reset");
                                $("#frm_answer_addedit").trigger("reset");
                                $('#addedit_modal').modal('hide');
                                $("#question_grid").bootgrid('reload');
                            }
                            , error: function (response) {
                                debugger;
                            }
                        });
                    }
                    else {
                        $.ajax({
                            type: "POST"
                            , url: "../answer-response.php"
                            , data: [{
                                name: "action"
                                , value: "deleteById"
                            }, {
                                name: "quesId"
                                , value: questionId
                            }]
                            , dataType: "json"
                            , success: function (response) {
                            }
                            , error: function (response) {
                            }
                        });
                        $("#frm_addedit").trigger("reset");
                        $("#frm_answer_addedit").trigger("reset");
                        $('#addedit_modal').modal('hide');
                        $("#question_grid").bootgrid('reload');
                    }
                }
                , error: function (response) {
                    debugger;
                }
            });
        }

        function hideShowForm(val) {
            switch (parseInt(val)) {
            case 1:
                $('#frm_answer_addedit').removeClass('hidden');
                answerAdded = true;
                break;
            case 2:
                $('#frm_answer_addedit').removeClass('hidden');
                answerAdded = true;
                break;
            case 3:
                $('#frm_answer_addedit').addClass('hidden');
                answerAdded = false;
                break;
            }
        }

        function addQuestion() {
            $('#addedit_modal').modal('show');
            hideShowForm("1");
            action = 'add';
        }

        function loadQuestions() {
            var grid = $("#question_grid").bootgrid({
                ajax: true
                , rowSelect: true
                , searchable: false
                , sortable: false
                , url: "../question-response.php"
                , formatters: {
                    "commands": function (column, row) {
                        return "<button type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\"" + row.id + "\"><span class=\"glyphicon glyphicon-edit\"></span></button> " + "<button type=\"button\" class=\"btn btn-xs btn-default command-delete\" data-row-id=\"" + row.id + "\"><span class=\"glyphicon glyphicon-trash\"></span></button>";
                    }
                }
            }).on("loaded.rs.jquery.bootgrid", function (response) {
                /* Executes after data is loaded and rendered */
                grid.find(".command-edit").on("click", function (e) {
                    debugger;
                    var ele = $(this).parent();
                    var g_id = $(this).parent().siblings(':first').html();
                    var g_name = $(this).parent().siblings(':nth-of-type(2)').html();
                    var g_type = $(this).parent().siblings(':nth-of-type(3)').html();
                    switch(g_type){
                        case "Single Choice":
                            g_type = 1;
                            break;
                        case "Multiple Choice":
                             g_type = 2;
                            break;
                        case "Range":
                             g_type = 3;
                            break;    
                    }
                    action = 'edit';
                    $('#frm_addedit #action').val('edit');
                    debugger;
                    if (parseInt(g_id) > 0) {
                        // collect the data
                        $('#addedit_id').val(g_id);
                        $('#addedit_question').val(g_name);
                        $('#addedit_type').val(g_type);
                        hideShowForm(g_type);
                        $.post("../answer-response.php", [{
                            name: "action"
                            , value: "getById"
                        }, {
                            name: "quesId"
                            , value: parseInt(g_id)
                        }], function (response) {
                            answerResponse = jQuery.parseJSON(response);
                            if (answerResponse != null) {
                                
                                $('#answerId1').val(answerResponse[0].Id);
                                $('#answer1').val(answerResponse[0].Description);
                                $('#weight1').val(answerResponse[0].Weight);
                                $('#answerId2').val(answerResponse[1].Id);
                                $('#answer2').val(answerResponse[1].Description);
                                $('#weight2').val(answerResponse[1].Weight);
                                $('#answerId3').val(answerResponse[2].Id);
                                $('#answer3').val(answerResponse[2].Description);
                                $('#weight3').val(answerResponse[2].Weight);
                                $('#answerId4').val(answerResponse[3].Id);
                                $('#answer4').val(answerResponse[3].Description);
                                $('#weight4').val(answerResponse[3].Weight);
                            }
                            if (parseInt(g_type) != 3) {
                                answerAdded = true;
                            }
                            else {
                                answerAdded = false;
                            }
                            $('#addedit_modal').modal('show');
                        });
                    }
                }).end().find(".command-delete").on("click", function (e) {
                    var ele = $(this).parent();
                    var g_id = $(this).parent().siblings(':first').html();
                    var g_name = $(this).parent().siblings(':nth-of-type(2)').html();
                    if (confirm('Are you sure to delete this question?')) {
                        $.post('../question-response.php', [{
                            name: "action"
                            , value: "delete"
                        }, {
                            name: "id"
                            , value: parseInt(g_id)
                        }], function (data) {
                            debugger;
                            $("#question_grid").bootgrid('reload');
                        });
                    }
                });
            });
        }
    </script>
    <?php
    include('../footer.php');
?>