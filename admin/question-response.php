
<?php
	//include connection file 
	include('session.php');
	
	$db = new dbObj();
	$connString =  $db->getConnstring();

	$params = $_REQUEST;
	
	$action = isset($params['action']) != '' ? $params['action'] : '';
	$quesCls = new Question($connString);

	switch($action) {
        case 'add':
            $quesCls->insertQuestion($params);
            break;
        case 'edit':
            $quesCls->updateQuestion($params);
            break;
        case 'delete':
            $quesCls->deleteQuestion($params);
            break;
        case 'getCategoryList':
            $quesCls->getQuestionCategoryList();
            break;
        case 'getTypeList':
            $quesCls->getQuestionTypeList();
            break;
        default:
            $quesCls->getQuestion($params);
            return;
	}
	
	class Question {
	protected $conn;
	protected $data = array();
	function __construct($connString) {
		$this->conn = $connString;
	}
	
	public function getQuestion($params) {
		
		$this->data = $this->getRecords($params);
		
		echo json_encode($this->data);
	}
        
    public function getQuestionCategoryList() {
		
		$sql = "SELECT Id, Category FROM QuestionCategory";
        
        $queryRecords = mysqli_query($this->conn, $sql);
        
        while( $row = mysqli_fetch_assoc($queryRecords) ) { 
			$data[] = $row;
        }
        
        echo json_encode($data);
	}
        
    public function getQuestionTypeList() {
		
		$sql = "SELECT Id, Type FROM QuestionType";
        
        $queryRecords = mysqli_query($this->conn, $sql);
        
        while( $row = mysqli_fetch_assoc($queryRecords) ) { 
			$data[] = $row;
        }
        
        echo json_encode($data);
	}
        
	function insertQuestion($params) {
		$data = array();
        
		$sql = "INSERT INTO Question (Question, Category, Type) VALUES('" . $params["addedit_question"] . "'," . $params["addedit_category"] . "," . $params["addedit_type"] . ")";
        
        if (mysqli_query($this->conn, $sql)) {
            $lastInsertId = mysqli_insert_id($this->conn);
            $sqInsert = "INSERT INTO SurveyQuestion (SurveyId, QuestionId) VALUES('" . $_SESSION['selected_survey'] . "'," . $lastInsertId . ");";
            
            mysqli_query($this->conn, $sqInsert);
            
            echo $lastInsertId;
        } else {
            echo mysqli_error($conn);
        }
	}
	
	function getRecords($params) {
		$rp = isset($params['rowCount']) ? $params['rowCount'] : 10;
		
		if (isset($params['current'])) { $page  = $params['current']; } else { $page=1; };  
        $start_from = ($page-1) * $rp;
		
		$sql = $sqlRec = $sqlTot = $where = '';
		

	   // getting total number records without any search
		$sql = "SELECT Question.Id AS Id, Question.Question AS Question, Question.Type AS Type, QuestionType.Type AS TypeName FROM Question INNER JOIN QuestionType ON Question.Type=QuestionType.Id JOIN SurveyQuestion ON Question.Id=SurveyQuestion.QuestionId WHERE SurveyQuestion.SurveyId =" . $_SESSION['selected_survey'];
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		if ($rp!=-1)
		$sqlRec .= " LIMIT ". $start_from .",".$rp;
		
		
		$qtot = mysqli_query($this->conn, $sqlTot) or die("error to fetch tot question data");
		$queryRecords = mysqli_query($this->conn, $sqlRec) or die("error to fetch question data");
		
		while( $row = mysqli_fetch_assoc($queryRecords) ) { 
			$data[] = $row;
		}
        
		$json_data = array(
			"current"            => intval($params['current']), 
			"rowCount"            => 10, 			
			"total"    => intval($qtot->num_rows),
			"rows"            => intval($qtot->num_rows) > 0 ? $data : []   // total data array
			);
		
		return $json_data;
	}
	function updateQuestion($params) {
		$data = array();
        
		$sql = "Update Question set Question = '" . $params["addedit_question"] . "', Type = " . $params["addedit_type"] . " WHERE Id=".$params["addedit_id"];
        
        if (mysqli_query($this->conn, $sql)) {
            echo $params["addedit_id"];
        } else {
            echo mysqli_error($conn);
        }
	}
	
	function deleteQuestion($params) {
		$data = array();
        
        $sql = "DELETE from Answer WHERE Answer.QuestionId = ".$params["id"];
        
        $result = mysqli_query($this->conn, $sql);
        
        $sql = "DELETE FROM SurveyQuestion WHERE SurveyQuestion.QuestionId =".$params["id"];
        
        $result = mysqli_query($this->conn, $sql);
        
        $sql = "DELETE from SurveyResult WHERE SurveyResult.QuestionId = ".$params["id"];
        
        $result = mysqli_query($this->conn, $sql);
        
        $sql = "delete from Question WHERE Id=".$params["id"];
		
		echo $result = mysqli_query($this->conn, $sql) or die("error to delete question");
        
	}
}
?>
	