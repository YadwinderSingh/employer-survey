-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 24, 2017 at 12:25 PM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `employertool`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `Id` int(11) NOT NULL,
  `Description` varchar(500) NOT NULL,
  `Weight` int(11) NOT NULL,
  `QuestionId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`Id`, `Description`, `Weight`, `QuestionId`) VALUES
(85, ' 0â€“10', 1, 89),
(86, ' 10â€“20', 1, 89),
(87, ' 20â€“30', 1, 89),
(88, ' 30â€“40', 1, 89),
(89, 'Van', 1, 90),
(90, 'SUV1', 1, 90),
(91, 'Sedan', 1, 90),
(92, 'none', 0, 90),
(93, 'connect', 1, 93),
(94, 'airtel', 1, 93),
(95, 'bsnl', 1, 93),
(96, 'aircel', 1, 93),
(99, 'ans1', 1, 95),
(101, 'ans2', 1, 95),
(103, 'ans3', 1, 95),
(104, 'ans4', 1, 95),
(105, 'ans1', 1, 96),
(106, 'ANS2', 1, 96),
(107, 'ANS3', 3, 96),
(108, 'ans4', 4, 96);

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `Id` int(11) NOT NULL,
  `Question` varchar(500) NOT NULL,
  `Type` int(11) NOT NULL,
  `Category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`Id`, `Question`, `Type`, `Category`) VALUES
(89, 'What is your age?', 1, 5),
(90, 'What type of vehicle do you own?', 2, 4),
(91, 'Range 1-10 select que1', 3, 5),
(92, 'Range 1-10 select que2', 3, 4),
(93, 'What is the fastest and most economical Internet service for you?', 2, 4),
(95, 'survey2 single', 1, 4),
(96, 'quesmulti1', 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `questioncategory`
--

CREATE TABLE `questioncategory` (
  `Id` int(11) NOT NULL,
  `Type` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questioncategory`
--

INSERT INTO `questioncategory` (`Id`, `Type`) VALUES
(4, 'Benefits'),
(5, 'Significant Impact');

-- --------------------------------------------------------

--
-- Table structure for table `questiontype`
--

CREATE TABLE `questiontype` (
  `Id` int(11) NOT NULL,
  `Type` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questiontype`
--

INSERT INTO `questiontype` (`Id`, `Type`) VALUES
(1, 'Single Choice'),
(2, 'Multiple Choice'),
(3, 'Range');

-- --------------------------------------------------------

--
-- Table structure for table `survey`
--

CREATE TABLE `survey` (
  `Id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `Created_Date` date DEFAULT NULL,
  `Created_By` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `survey`
--

INSERT INTO `survey` (`Id`, `name`, `Created_Date`, `Created_By`) VALUES
(8, 'Survey1', '2017-08-22', 2),
(9, 'survey2', '2017-08-23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `surveyquestion`
--

CREATE TABLE `surveyquestion` (
  `SurveyId` int(11) NOT NULL,
  `QuestionId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `surveyquestion`
--

INSERT INTO `surveyquestion` (`SurveyId`, `QuestionId`) VALUES
(8, 89),
(8, 90),
(8, 91),
(8, 92),
(8, 93),
(9, 95),
(9, 96);

-- --------------------------------------------------------

--
-- Table structure for table `surveyresult`
--

CREATE TABLE `surveyresult` (
  `Id` int(11) NOT NULL,
  `SurveyId` int(11) DEFAULT NULL,
  `AgeGroup` varchar(22) DEFAULT NULL,
  `CompletedByEmail` varchar(50) DEFAULT NULL,
  `Company` varchar(150) DEFAULT NULL,
  `DateAttended` date DEFAULT NULL,
  `Phone` varchar(50) DEFAULT NULL,
  `NoOfEmployees` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `surveyresult`
--

INSERT INTO `surveyresult` (`Id`, `SurveyId`, `AgeGroup`, `CompletedByEmail`, `Company`, `DateAttended`, `Phone`, `NoOfEmployees`) VALUES
(60, 8, '38-50', 'vashishtsunil007@gmail.com', 'test', '2017-08-23', NULL, '10-19'),
(61, 8, '18-28', 'dev-mcrg@serenetechnologies.ca', 'test', '2017-08-23', NULL, '10-19'),
(62, 8, '18-28', 'dev-mcrg@serenetechnologies.ca', 'test', '2017-08-23', NULL, '10-19'),
(63, 8, '18-28', 'dev-mcrg@serenetechnologies.ca', 'test', '2017-08-23', NULL, '10-19'),
(64, 8, '28-38', 'aroraneeru011@gmail.com', 'admin', '2017-08-23', NULL, '20-29'),
(65, 8, '28-38', 'aroraneeru011@gmail.com', 'admin', '2017-08-23', NULL, '20-29'),
(66, 8, '28-38', 'aroraneeru011@gmail.com', 'admin', '2017-08-23', NULL, '20-29'),
(67, 8, '28-38', 'aroraneeru011@gmail.com', 'admin', '2017-08-23', NULL, '20-29'),
(68, 8, '28-38', 'aroraneeru011@gmail.com', 'admin', '2017-08-23', NULL, '20-29'),
(69, 8, '28-38', 'aroraneeru011@gmail.com', 'admin', '2017-08-23', NULL, '20-29'),
(70, 8, '28-38', 'dev-mcrg@serenetechnologies.ca', 'test', '2017-08-23', NULL, '1-9'),
(71, 8, '28-38', 'dev-mcrg@serenetechnologies.ca', 'test', '2017-08-23', NULL, '1-9'),
(72, 8, '18-28', 'aroraneeru011@gmail.com', 'test', '2017-08-23', NULL, '10-19'),
(73, 8, '18-28', 'aroraneeru011@gmail.com', 'test', '2017-08-23', NULL, '10-19'),
(74, 8, '18-28', 'aroraneeru011@gmail.com', 'test', '2017-08-23', NULL, '10-19'),
(75, 8, '18-28', 'aroraneeru011@gmail.com', 'test', '2017-08-23', NULL, '10-19'),
(76, 8, '18-28', 'aroraneeru011@gmail.com', 'test', '2017-08-23', NULL, '10-19'),
(77, 8, '18-28', 'aroraneeru011@gmail.com', 'test', '2017-08-23', NULL, '1-9'),
(78, 8, '28-38', 'aroraneeru011@gmail.com', 'test', '2017-08-23', NULL, '10-19'),
(79, 8, '28-38', 'dev-mcrg@serenetechnologies.ca', 'test', '2017-08-23', NULL, '1-9'),
(80, 8, '28-38', 'aroraneeru011@gmail.com', 'hjghjgjhghj', '2017-08-23', NULL, '10-19'),
(81, 8, '28-38', 'aroraneeru011@gmail.com', 'test', '2017-08-23', NULL, '20-29'),
(82, 8, '18-28', 'aroraneeru011@gmail.com', 'test', '2017-08-23', NULL, '10-19'),
(83, 8, '38-50', 'sunil@serenetechnologies.ca', 'admin', '2017-08-23', NULL, '20-29'),
(84, 8, '18-28', 'dev-mcrg@serenetechnologies.ca', 'test', '2017-08-23', NULL, '10-19'),
(85, 8, '28-38', 'dev-mcrg@serenetechnologies.ca', 'test', '2017-08-23', NULL, '20-29'),
(86, 8, NULL, 'vashishtsunil007@gmail.com', 'admin', '2017-08-24', '8767866', '54'),
(87, 8, NULL, '', '', '2017-08-24', '', ''),
(88, 8, NULL, 'vashishtsunil007@gmail.com', 'test', '2017-08-24', '8767866', '2');

-- --------------------------------------------------------

--
-- Table structure for table `surveyresultdetails`
--

CREATE TABLE `surveyresultdetails` (
  `Id` int(11) NOT NULL,
  `SurveyResultId` int(11) NOT NULL,
  `QuesId` int(11) DEFAULT NULL,
  `AnsId` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surveyresultdetails`
--

INSERT INTO `surveyresultdetails` (`Id`, `SurveyResultId`, `QuesId`, `AnsId`, `CreatedOn`) VALUES
(1, 60, 91, 2, '2017-08-23 14:51:17'),
(2, 60, 92, 4, '2017-08-23 14:51:17'),
(3, 60, 90, 89, '2017-08-23 14:51:17'),
(4, 60, 90, 90, '2017-08-23 14:51:18'),
(5, 60, 90, 91, '2017-08-23 14:51:18'),
(6, 60, 93, 93, '2017-08-23 14:51:18'),
(7, 60, 93, 94, '2017-08-23 14:51:18'),
(8, 60, 89, 85, '2017-08-23 14:51:18'),
(9, 61, 91, 7, '2017-08-23 14:55:02'),
(10, 61, 92, 3, '2017-08-23 14:55:03'),
(11, 61, 90, 91, '2017-08-23 14:55:03'),
(12, 61, 93, 93, '2017-08-23 14:55:03'),
(13, 61, 89, 85, '2017-08-23 14:55:03'),
(14, 62, 91, 7, '2017-08-23 14:56:12'),
(15, 62, 92, 3, '2017-08-23 14:56:12'),
(16, 62, 90, 91, '2017-08-23 14:56:12'),
(17, 62, 93, 93, '2017-08-23 14:56:12'),
(18, 62, 89, 85, '2017-08-23 14:56:12'),
(19, 63, 91, 7, '2017-08-23 14:56:37'),
(20, 63, 92, 3, '2017-08-23 14:56:37'),
(21, 63, 90, 91, '2017-08-23 14:56:37'),
(22, 63, 93, 93, '2017-08-23 14:56:37'),
(23, 63, 89, 85, '2017-08-23 14:56:37'),
(24, 64, 91, 6, '2017-08-23 14:59:04'),
(25, 64, 92, 5, '2017-08-23 14:59:04'),
(26, 64, 90, 89, '2017-08-23 14:59:04'),
(27, 64, 90, 90, '2017-08-23 14:59:04'),
(28, 64, 90, 91, '2017-08-23 14:59:04'),
(29, 64, 93, 96, '2017-08-23 14:59:04'),
(30, 64, 89, 85, '2017-08-23 14:59:04'),
(31, 65, 91, 6, '2017-08-23 15:00:34'),
(32, 65, 92, 5, '2017-08-23 15:00:34'),
(33, 65, 90, 89, '2017-08-23 15:00:34'),
(34, 65, 90, 90, '2017-08-23 15:00:34'),
(35, 65, 90, 91, '2017-08-23 15:00:34'),
(36, 65, 93, 96, '2017-08-23 15:00:34'),
(37, 65, 89, 85, '2017-08-23 15:00:34'),
(38, 66, 91, 6, '2017-08-23 15:17:55'),
(39, 66, 92, 5, '2017-08-23 15:17:56'),
(40, 66, 90, 89, '2017-08-23 15:17:56'),
(41, 66, 90, 90, '2017-08-23 15:17:56'),
(42, 66, 90, 91, '2017-08-23 15:17:56'),
(43, 66, 93, 96, '2017-08-23 15:17:56'),
(44, 66, 89, 85, '2017-08-23 15:17:56'),
(45, 68, 91, 6, '2017-08-23 15:19:11'),
(46, 68, 92, 5, '2017-08-23 15:19:12'),
(47, 68, 90, 89, '2017-08-23 15:19:12'),
(48, 68, 90, 90, '2017-08-23 15:19:12'),
(49, 68, 90, 91, '2017-08-23 15:19:12'),
(50, 68, 93, 96, '2017-08-23 15:19:12'),
(51, 68, 89, 85, '2017-08-23 15:19:12'),
(52, 69, 91, 6, '2017-08-23 15:20:22'),
(53, 69, 92, 5, '2017-08-23 15:20:22'),
(54, 69, 90, 89, '2017-08-23 15:20:22'),
(55, 69, 90, 90, '2017-08-23 15:20:22'),
(56, 69, 90, 91, '2017-08-23 15:20:22'),
(57, 69, 93, 96, '2017-08-23 15:20:22'),
(58, 69, 89, 85, '2017-08-23 15:20:22'),
(59, 70, 91, 5, '2017-08-23 16:20:31'),
(60, 70, 92, 2, '2017-08-23 16:20:31'),
(61, 70, 90, 89, '2017-08-23 16:20:31'),
(62, 70, 93, 93, '2017-08-23 16:20:31'),
(63, 70, 89, 85, '2017-08-23 16:20:31'),
(64, 71, 91, 5, '2017-08-23 16:22:51'),
(65, 71, 92, 2, '2017-08-23 16:22:51'),
(66, 71, 90, 89, '2017-08-23 16:22:51'),
(67, 71, 93, 93, '2017-08-23 16:22:51'),
(68, 71, 89, 85, '2017-08-23 16:22:51'),
(69, 72, 91, 2, '2017-08-23 16:25:24'),
(70, 72, 92, 3, '2017-08-23 16:25:24'),
(71, 72, 90, 89, '2017-08-23 16:25:24'),
(72, 72, 93, 93, '2017-08-23 16:25:24'),
(73, 73, 91, 2, '2017-08-23 16:27:23'),
(74, 73, 92, 3, '2017-08-23 16:27:23'),
(75, 73, 90, 89, '2017-08-23 16:27:23'),
(76, 73, 93, 93, '2017-08-23 16:27:23'),
(77, 73, 89, 85, '2017-08-23 16:27:23'),
(78, 74, 91, 2, '2017-08-23 16:30:10'),
(79, 74, 92, 3, '2017-08-23 16:30:10'),
(80, 74, 90, 89, '2017-08-23 16:30:10'),
(81, 74, 93, 93, '2017-08-23 16:30:10'),
(82, 74, 89, 85, '2017-08-23 16:30:10'),
(83, 75, 91, 2, '2017-08-23 16:37:58'),
(84, 75, 92, 3, '2017-08-23 16:37:58'),
(85, 75, 90, 89, '2017-08-23 16:37:58'),
(86, 75, 93, 93, '2017-08-23 16:37:58'),
(87, 75, 89, 85, '2017-08-23 16:37:58'),
(88, 76, 91, 2, '2017-08-23 16:38:59'),
(89, 76, 92, 3, '2017-08-23 16:38:59'),
(90, 76, 90, 89, '2017-08-23 16:38:59'),
(91, 76, 93, 93, '2017-08-23 16:38:59'),
(92, 76, 89, 85, '2017-08-23 16:38:59'),
(93, 77, 91, 8, '2017-08-23 16:47:49'),
(94, 77, 92, 6, '2017-08-23 16:47:49'),
(95, 77, 90, 90, '2017-08-23 16:47:49'),
(96, 77, 93, 93, '2017-08-23 16:47:49'),
(97, 77, 89, 85, '2017-08-23 16:47:49'),
(98, 78, 91, 6, '2017-08-23 16:48:45'),
(99, 78, 92, 2, '2017-08-23 16:48:45'),
(100, 78, 90, 89, '2017-08-23 16:48:45'),
(101, 78, 90, 92, '2017-08-23 16:48:45'),
(102, 78, 93, 93, '2017-08-23 16:48:45'),
(103, 78, 89, 86, '2017-08-23 16:48:45'),
(104, 79, 91, 4, '2017-08-23 16:51:04'),
(105, 79, 92, 3, '2017-08-23 16:51:04'),
(106, 79, 90, 89, '2017-08-23 16:51:04'),
(107, 79, 93, 95, '2017-08-23 16:51:04'),
(108, 79, 89, 87, '2017-08-23 16:51:04'),
(109, 80, 91, 0, '2017-08-23 16:56:32'),
(110, 80, 92, 8, '2017-08-23 16:56:32'),
(111, 80, 90, 90, '2017-08-23 16:56:32'),
(112, 80, 93, 95, '2017-08-23 16:56:32'),
(113, 80, 89, 85, '2017-08-23 16:56:32'),
(114, 81, 91, 2, '2017-08-23 17:00:11'),
(115, 81, 92, 3, '2017-08-23 17:00:11'),
(116, 81, 90, 89, '2017-08-23 17:00:11'),
(117, 81, 90, 90, '2017-08-23 17:00:11'),
(118, 81, 93, 94, '2017-08-23 17:00:11'),
(119, 81, 89, 85, '2017-08-23 17:00:11'),
(120, 82, 91, 7, '2017-08-23 17:09:00'),
(121, 82, 92, 7, '2017-08-23 17:09:00'),
(122, 82, 90, 91, '2017-08-23 17:09:00'),
(123, 82, 93, 93, '2017-08-23 17:09:00'),
(124, 82, 89, 86, '2017-08-23 17:09:00'),
(125, 83, 91, 3, '2017-08-23 17:11:01'),
(126, 83, 92, 7, '2017-08-23 17:11:01'),
(127, 83, 90, 90, '2017-08-23 17:11:01'),
(128, 83, 90, 91, '2017-08-23 17:11:01'),
(129, 83, 93, 94, '2017-08-23 17:11:01'),
(130, 83, 93, 95, '2017-08-23 17:11:01'),
(131, 83, 89, 87, '2017-08-23 17:11:01'),
(132, 84, 91, 4, '2017-08-23 17:15:24'),
(133, 84, 92, 3, '2017-08-23 17:15:24'),
(134, 84, 90, 89, '2017-08-23 17:15:24'),
(135, 84, 90, 90, '2017-08-23 17:15:24'),
(136, 84, 93, 93, '2017-08-23 17:15:24'),
(137, 84, 89, 85, '2017-08-23 17:15:24'),
(138, 85, 91, 3, '2017-08-23 17:18:14'),
(139, 85, 92, 5, '2017-08-23 17:18:14'),
(140, 85, 90, 89, '2017-08-23 17:18:14'),
(141, 85, 90, 90, '2017-08-23 17:18:14'),
(142, 85, 90, 91, '2017-08-23 17:18:14'),
(143, 85, 90, 92, '2017-08-23 17:18:14'),
(144, 85, 93, 93, '2017-08-23 17:18:14'),
(145, 85, 93, 94, '2017-08-23 17:18:14'),
(146, 85, 93, 95, '2017-08-23 17:18:14'),
(147, 85, 89, 85, '2017-08-23 17:18:14'),
(148, 0, 91, 2, '2017-08-24 16:28:43'),
(149, 0, 92, 4, '2017-08-24 16:28:43'),
(150, 0, 90, 89, '2017-08-24 16:28:43'),
(151, 0, 93, 93, '2017-08-24 16:28:43'),
(152, 0, 89, 85, '2017-08-24 16:28:43'),
(153, 86, 91, 2, '2017-08-24 16:32:08'),
(154, 86, 92, 4, '2017-08-24 16:32:08'),
(155, 86, 90, 89, '2017-08-24 16:32:08'),
(156, 86, 93, 93, '2017-08-24 16:32:08'),
(157, 86, 89, 85, '2017-08-24 16:32:08'),
(158, 87, 91, 3, '2017-08-24 16:53:43'),
(159, 87, 92, 4, '2017-08-24 16:53:43'),
(160, 87, 90, 89, '2017-08-24 16:53:43'),
(161, 87, 93, 94, '2017-08-24 16:53:43'),
(162, 87, 89, 85, '2017-08-24 16:53:43'),
(163, 88, 91, 5, '2017-08-24 17:52:55'),
(164, 88, 92, 3, '2017-08-24 17:52:55'),
(165, 88, 90, 89, '2017-08-24 17:52:55'),
(166, 88, 90, 90, '2017-08-24 17:52:55'),
(167, 88, 90, 91, '2017-08-24 17:52:55'),
(168, 88, 90, 92, '2017-08-24 17:52:55'),
(169, 88, 93, 96, '2017-08-24 17:52:55'),
(170, 88, 89, 85, '2017-08-24 17:52:55');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `Id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `passcode` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`Id`, `username`, `Email`, `passcode`) VALUES
(1, 'admin', 'admin@admin.com', 'admin'),
(2, 'gurjit', 'g@g.com', 'gurjit');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answer`
--
ALTER TABLE `answer`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `QuestionId` (`QuestionId`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Type` (`Type`),
  ADD KEY `Category` (`Category`);

--
-- Indexes for table `questioncategory`
--
ALTER TABLE `questioncategory`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `questiontype`
--
ALTER TABLE `questiontype`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `survey`
--
ALTER TABLE `survey`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Created_By` (`Created_By`);

--
-- Indexes for table `surveyquestion`
--
ALTER TABLE `surveyquestion`
  ADD KEY `SurveyId` (`SurveyId`),
  ADD KEY `QuestionId` (`QuestionId`);

--
-- Indexes for table `surveyresult`
--
ALTER TABLE `surveyresult`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `SurveyId` (`SurveyId`);

--
-- Indexes for table `surveyresultdetails`
--
ALTER TABLE `surveyresultdetails`
  ADD KEY `auto_index_key` (`Id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answer`
--
ALTER TABLE `answer`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `questioncategory`
--
ALTER TABLE `questioncategory`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `questiontype`
--
ALTER TABLE `questiontype`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `survey`
--
ALTER TABLE `survey`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `surveyresult`
--
ALTER TABLE `surveyresult`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `surveyresultdetails`
--
ALTER TABLE `surveyresultdetails`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `answer`
--
ALTER TABLE `answer`
  ADD CONSTRAINT `Questions_Answer` FOREIGN KEY (`QuestionId`) REFERENCES `question` (`Id`);

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `QuestionCategory_Question` FOREIGN KEY (`Category`) REFERENCES `questioncategory` (`Id`),
  ADD CONSTRAINT `QuestionType_Question` FOREIGN KEY (`Type`) REFERENCES `questiontype` (`Id`);

--
-- Constraints for table `survey`
--
ALTER TABLE `survey`
  ADD CONSTRAINT `User_survey` FOREIGN KEY (`Created_By`) REFERENCES `user` (`Id`);

--
-- Constraints for table `surveyquestion`
--
ALTER TABLE `surveyquestion`
  ADD CONSTRAINT `Question_SurveyQuestion` FOREIGN KEY (`QuestionId`) REFERENCES `question` (`Id`),
  ADD CONSTRAINT `Survey_SurveyQuestion` FOREIGN KEY (`SurveyId`) REFERENCES `survey` (`Id`);

--
-- Constraints for table `surveyresult`
--
ALTER TABLE `surveyresult`
  ADD CONSTRAINT `Survey_SurveyResult` FOREIGN KEY (`SurveyId`) REFERENCES `survey` (`Id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
