<?php
    include('../header.php');
   $ses_sql = mysqli_query($connString,"SELECT count(*) as surveys FROM surveyresult WHERE DateAttended > DATE_SUB(NOW(), INTERVAL 1 DAY)");   
   $row = mysqli_fetch_array($ses_sql,MYSQLI_ASSOC);   
   $todaySurvey = $row['surveys'];
   
   $ses_sql = mysqli_query($connString,"SELECT count(*) as surveys FROM surveyresult WHERE DateAttended > DATE_SUB(NOW(), INTERVAL 1 WEEK)");   
   $row = mysqli_fetch_array($ses_sql,MYSQLI_ASSOC);   
   $weekSurvey = $row['surveys'];
   
   
   $ses_sql = mysqli_query($connString,"SELECT count(*) as surveys FROM surveyresult WHERE DateAttended > DATE_SUB(NOW(), INTERVAL 1 MONTH)");   
   $row = mysqli_fetch_array($ses_sql,MYSQLI_ASSOC);   
   $monthSurvey = $row['surveys'];
    
?>
    <style type="text/css">
        .actionBar {
            display: none
        }
    </style>
    <div class="wrapper wrapper-content">
        <div class="container">
            <div class="row">
                 <div class="col-lg-4  col-md-4">
                    <div class="ibox float-e-margins">
                        
                        <div class="ibox-content">
                            <div class="form-group">
                                <h4 class="text-info">No Of Surveys Submitted Today: <?php echo $todaySurvey;?></h4>
                                
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
            
            
            
            <div class="col-lg-4  col-md-4">
                    <div class="ibox float-e-margins">
                        
                        <div class="ibox-content">
                            <div class="form-group">
                                <h4 class="text-info">No Of Surveys Submitted Week: <?php echo $weekSurvey;?></h4>
                                
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
                
                
                <div class="col-lg-4  col-md-4">
                    <div class="ibox float-e-margins">
                        
                        <div class="ibox-content">
                            <div class="form-group">
                                <h4 class="text-info">No Of Surveys Submitted Month: <?php echo $monthSurvey;?></h4>
                                
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
    <?php
    include('../footer.php');
?>