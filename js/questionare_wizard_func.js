/*  Wizard */
var score = 0;
jQuery(function ($) {
	"use strict";
	$('form#wrapped').attr('action', 'questionare_send.php');
	$("#wizard_container").wizard({
		stepsWrapper: "#wrapped",
		submit: ".submit",
		beforeSelect: function (event, state) {
			if (!state.isMovingForward)
				return true;
			var inputs = $(this).wizard('state').step.find(':input');
			return !inputs.length || !!inputs.valid();
		}
	}).wizard("form").submit(function (event) {
		event.preventDefault();		
		var data = parseParams($('#survey_form').serialize());	
		 debugger;
		$.post("server.php", data, function(resultResponse) {	
			debugger;	 
			$("#wizard_container").addClass('hidden');
			$("#success").removeClass('hidden');
        
            var result="";
			 var finalData= JSON.parse(resultResponse); 
			 debugger;
         
			$.each(finalData, function( index, value ) {
				var cat_name="";
				var cat_total=0;
				var cat_obt=0;
				
				$.each(value, function( index, cat ) {
					cat_name=cat['CatName'];
					
					cat_total= cat_total+cat['Total'];
					cat_obt= cat_obt+cat['Obtained'];
				});
				
				result +="<p class='txt-success'>Cat Name : "+cat_name+" , Total Weight: "+cat_total+"  , Achieved Weight : "+cat_obt +"</p>";
			});
				
			$("#total").html(result);
		}).fail(function(response) {
			debugger;
		});

	}).validate({
		errorPlacement: function (error, element) {
			if (element.is(':radio') || element.is(':checkbox')) {
				error.insertBefore(element.next());
			} else {
				error.insertAfter(element);
			}
		}
	});
	//  progress bar
	$("#progressbar").progressbar();
	$("#wizard_container").wizard({
		afterSelect: function (event, state) {
			$("#progressbar").progressbar("value", state.percentComplete);
			$("#location").text("(" + state.stepsComplete + "/" + state.stepsPossible + ")");
		}
	});
});

function parseParams(str) {
	var result = [{
		name: "action",
		value: "insertSurvey"
	}];
    var count=0;
	var params = str.split('&');
	params.forEach(function(param) {
	   
    
		var paramSplit = param.split('=').map(function (value) {
			return decodeURIComponent(value.replace('+', ' '));
		});

		if(paramSplit[0].indexOf("question_") !== -1){
		  
			paramSplit[0] = paramSplit[0].replace('question_', '');
			paramSplit[0] = paramSplit[0].replace('[]', '');
            paramSplit[0]=count+"-"+paramSplit[0];
			if($('#ques_' + paramSplit[0]).length > 0 && $('#ques_' + paramSplit[0]).attr('data-weight') != "" ){
				score += parseInt($('#ques_' + paramSplit[0]).attr('data-weight'));
			}
			else{
				score += parseInt(paramSplit[1]);
			}

			var finalParam = {
				name: paramSplit[0],
				value: paramSplit[1]
			};
	
			result.push(finalParam);
		}
		else{
			var finalParam = {
				name: paramSplit[0],
				value: paramSplit[1]
			};
	
			result.unshift(finalParam);
		}
		
        count++;

	}, this);
	
	return result;
}